/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
function getMindandSpace(board,list){
    return new Promise((reslove,reject)=>{
        setTimeout(() => {
            let id=""
   board.reduce((cv) => {
    if(cv.name==="Thanos"){
        id=cv.id
    }
   })
   let mindid = [];

   for (let lists in list) {
       for (let listses of list[lists]) {
           if (listses.name === 'Mind' || listses.name === 'Space') {
               mindid.push(listses.id);
           }
       }
   }
   
   if (!id || !mindid) {
    reject(new Error('Required IDs not found'));
} else {
    reslove({ id, mindid });
}

    })
            
        }, 3000);
    
}



module.exports={getMindandSpace}
