function cardsinformation(cards, listsid) {
    return new Promise((resolve, reject) => {
        const cardsdata = cards[listsid];
        if (cardsdata) {
            resolve(cardsdata);
        } else {
            const errorMessage = "Card not found for listsid: " + listsid;
            console.log(errorMessage);
            reject(errorMessage);
        }
    });
}

module.exports = {
    cardsinformation: cardsinformation,
};
