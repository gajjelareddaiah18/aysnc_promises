const {getMindandSpace}=require("../callback5")
const { board_information} = require("../callback1");
const list = require("../lists_1.json"); 
const {listvalue,callbacks}=require("../callback2")
const board = require("../boards_1.json");
const cards = require("../cards_1.json");
const { cardsinformation} = require("../callback3");

getMindandSpace(board, list, cards)
.then(({ id, mindid }) => {
    const promises = [
        board_information(board, id),
        listvalue(list, id),
        ...mindid.map((mind) => cardsinformation(cards, mind))
    ];
    return Promise.all(promises);
})
.then(([boardInfo, listValue, ...cardsInfo]) => {
    console.log('Board Information:', boardInfo);
    console.log('List Value:', listValue);
    console.log('Cards Information:', cardsInfo);
})
.catch(error => {
    console.error(error.message);
});



