const { allgetnames } = require("../callback6");
const { board_information } = require("../callback1");
const list = require("../lists_1.json");
const { listvalue } = require("../callback2");
const board = require("../boards_1.json");
const cards = require("../cards_1.json");
const { cardsinformation } = require("../callback3");

allgetnames(board, list)
    .then(({ id, listids }) => {
        const boardPromise = board_information(board, id);
        const listPromise = listvalue(list, id);
        const cardsPromises = listids.map(listids1 => cardsinformation(cards, listids1));
        cardsPromises.pop()
        return Promise.all([boardPromise, listPromise, ...cardsPromises]);
    })
    .then(([boardInfo, listValue, ...cardsInfo]) => {
        console.log('Board Information:', boardInfo);
        console.log('List Value:', listValue);
        console.log('Cards Information:', cardsInfo);
    })
    .catch(error => {
        console.error(error.message);
    });
