const board = require("../boards_1.json");
const { board_information } = require("../callback1");


board_information(board, 'mcu453ed')
    .then(foundBoard => {
        console.log(foundBoard);
    })
    .catch(error => {
        console.error(error.message);
    });