
const { board_information } = require("../callback1");
const list = require("../lists_1.json"); 
const {listvalue}=require("../callback2")
const board = require("../boards_1.json");
const cards = require("../cards_1.json");
const { cardsinformation} = require("../callback3");
const {getInfoMind}=require("../callback4")

getInfoMind(board, list, cards)
.then(({ id, mindid }) => {
    return Promise.all([
        board_information(board, id),
        listvalue(list, id),
        cardsinformation(cards, mindid)
    ]);
})
.then(([boardInfo, listValue, cardsInfo]) => {
    console.log('Board Information:', boardInfo);
    console.log('List Value:', listValue);
    console.log('Cards Information:', cardsInfo);
})
.catch(error => {
    console.error(error.message);
});
    
