function allgetnames(board, list) {
    return new Promise((resolve, reject) => {
       setTimeout(() => {
        let id = "";
        board.forEach((cv) => {
            if (cv.name === "Thanos") {
                id = cv.id;
            }
        });

        let listids = [];
        for (let lists in list) {
            if(lists==id){
                for(let listid of list[lists]){
                    listids.push(listid.id)
                }
            }
        
                    
                }     

        if (!id || !listids) { // Check if listids array is empty
            reject(new Error("Required IDs not found"));
        } else {
            resolve({ id, listids });
        }
    });
        
       }, 3000);
}

module.exports = { allgetnames };
